from NeuronalNetwork import *
if __name__ == "__main__":

    nn = NeuralNetwork([64, 100, 1], activation='logistic')

    temp = np.genfromtxt('digits.csv.gz', delimiter=',')
    X, y = temp[:, :-1], temp[:, -1]

    nn.fit(X, y, 0.1, 100)
    plot_digit(X, y, rand.randint(0, len(X)))
